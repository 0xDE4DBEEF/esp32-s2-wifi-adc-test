#ifndef PW_WEBSERVER_H
#define PW_WEBSERVER_H

#include <Arduino.h>
#include "ESPAsyncWebServer.h" 
#include "SPIFFS.h"

void initWebserver();
String processor(const String& var);

#endif