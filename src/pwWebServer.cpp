#include "pwWebServer.h"

AsyncWebServer server(80);

void initWebserver() {
    Serial.println("initWebserver()");

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        Serial.println("server.on(\"/\")");
        request->send(SPIFFS, "/index.html", String(), false, processor);
    });

    server.on("/main.css", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/main.css", "text/css");
    });

    server.onNotFound([](AsyncWebServerRequest *request) {
        request->send(404, "text/plain", "Not found");
    });

    server.begin();

    Serial.println("webserver started");
}

String processor(const String& var){
    return var;
}