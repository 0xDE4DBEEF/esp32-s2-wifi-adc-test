#include <Arduino.h>
#include <WiFi.h>
#include "WiFiManager.h"
#include "pwWebServer.h"
#include "pwADC.h"

bool connectViaWiFiManager() {
    WiFi.mode(WIFI_AP);
    WiFi.disconnect();
    
    WiFiManager wm;

    bool res;
    wm.setConnectRetries(2);
    return wm.autoConnect("AutoConnectAP"); 
}

bool connectWiFiManually() {
    WiFi.mode(WIFI_STA);
    WiFi.begin("WIFISSID", "WIFIPASS");
    Serial.print("Connecting to WiFi ..");
    int startMillis = millis();
    const int conTimeOut = 10000; // timeout after 10 seconds
    while (WiFi.status() != WL_CONNECTED) {
        delay(1);

        if((millis() - startMillis) > conTimeOut) {
            Serial.println(". connection timed out ...");
            return false;
        }
    }

    int timePassed = millis() - startMillis;
    Serial.printf("connection took %.2f seconds\n", ((float)timePassed/1000.0));
    Serial.println(WiFi.localIP());

    return true;
}

void setup() {
    Serial.begin(115200);
    delay(900);

    //if(!connectWiFiManually()) {
    if(!connectViaWiFiManager()) {
        Serial.println("Failed to connect, restarting");
        ESP.restart();
    } 
    else {
        //if you get here you have connected to the WiFi    
        Serial.println("connected...yeey :)");

        if(!SPIFFS.begin(true)) {
            Serial.println("An Error has occurred while mounting SPIFFS");
            return;
        } else {
            initWebserver();
            initPwADCTask();    // multisample ADC in separate task
            // configureADC();  // uncomment for single read
        }
    }

    // singleReadADC(); // uncomment for single read before first loop() call
}

void loop() {
    // quick and very dirty code to reset wifimanager credentials with io0
    if (digitalRead(0) == LOW) {
        delay(500);
        if (digitalRead(0) == LOW) {
            Serial.println("io0 held for half a second, resetting wifi");
            WiFi.disconnect(true, true);
            delay(1000);
        }
    }

    // singleReadADC(); // uncomment for single read in each loop

    if (!WiFi.isConnected()) {
        Serial.println("WiFi has lost connection, trying to reconnect");
        //if(!connectWiFiManually()) {
        if(!connectViaWiFiManager()) {
            Serial.println("Connection failed, restarting");
            ESP.restart();
        }
    }

    // Wait a bit before scanning again
    delay(1000);
}