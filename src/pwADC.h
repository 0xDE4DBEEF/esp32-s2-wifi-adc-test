#ifndef pw_adc_h
#define pw_adc_h

#include "driver/adc.h"
#include "esp_adc_cal.h"

void initPwADCTask();
void updateADCTask(void *p);
void singleReadADC();
void configureADC();
static void check_efuse(void);
static void print_char_val_type(esp_adc_cal_value_t val_type);

#endif