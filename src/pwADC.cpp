#include <Arduino.h>
#include "pwADC.h"

extern volatile float curSensorReading_adci;

#define DEFAULT_VREF    1100        //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES   64          //Multisampling

static esp_adc_cal_characteristics_t *adc_chars;
static const adc_channel_t adcChannel = ADC_CHANNEL_9;    // use any channel on ADC1, no difference on test-pcb
static const adc_bits_width_t adcWidth = ADC_WIDTH_BIT_13;
static const adc_atten_t atten = ADC_ATTEN_DB_0;
static const adc_unit_t unit = ADC_UNIT_1;

void initPwADCTask(void) {
    Serial.println("Setting up ADC task");

    configureADC();

    xTaskCreate(
        updateADCTask,    // Function that should be called
        "updateADCTask",   // Name of the task (for debugging)
        16000,            // Stack size (bytes)
        NULL,            // Parameter to pass
        1,               // Task priority
        NULL             // Task handle
    );
}

void updateADCTask(void *p) {
    while(1) {
        uint32_t adc_reading = 0;
        //Multisampling
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
            adc_reading += adc1_get_raw((adc1_channel_t)adcChannel);
        }
        adc_reading /= NO_OF_SAMPLES;

        //Convert adc_reading to voltage in mV
        uint32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
        Serial.printf("Raw: %d\tVoltage: %dmV\n", adc_reading, voltage);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void singleReadADC() {
    uint32_t adcReading = adc1_get_raw((adc1_channel_t)adcChannel);
    uint32_t voltage = esp_adc_cal_raw_to_voltage(adcReading, adc_chars);
    Serial.printf("Raw: %d\tVoltage: %dmV\n", adcReading, voltage);
}

void configureADC() {
    Serial.println("configuring ADC");
    
    //Check if Two Point or Vref are burned into eFuse
    check_efuse();

    adc1_config_width(adcWidth);
    adc1_config_channel_atten((adc1_channel_t)adcChannel, atten);

    //Characterize ADC
    adc_chars = (esp_adc_cal_characteristics_t *) calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, adcWidth, DEFAULT_VREF, adc_chars);
    print_char_val_type(val_type);
}

static void check_efuse(void)
{
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
        printf("eFuse Two Point: Supported\n");
    } else {
        printf("Cannot retrieve eFuse Two Point calibration values. Default calibration values will be used.\n");
    }
}

static void print_char_val_type(esp_adc_cal_value_t val_type)
{
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        printf("Characterized using Two Point Value\n");
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        printf("Characterized using eFuse Vref\n");
    } else {
        printf("Characterized using Default Vref\n");
    }
}