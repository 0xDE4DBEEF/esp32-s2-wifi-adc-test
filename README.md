# ESP32-S2 ADC + WiFi Problem

I am using an ESP32-S2FH4 on a custom pcb based on the ESP32-S2-WROOM schematic. This project serves to illustrate a problem I am having while simultaniously using WiFi and ADC. 

Whenever I connect to WiFi and then perform an ADC conversion, the WiFi connection will drop out shortly after, and WiFi will not be able to reconnect using the code in the Project, unless the chip is reset by pulling CHIP_PU low. 

Serial Output on the test PCB is as follows: 
```
ESP-ROM:esp32s2-rc4-20191025
Build:Oct 25 2019
rst:0x1 (POWERON),boot:0x8 (SPI_FAST_FLASH_BOOT)
SPIWP:0xee
mode:DIO, clock div:1
load:0x3ffe6100,len:0x8
load:0x3ffe6108,len:0x620
load:0x4004c000,len:0xa40
load:0x40050000,len:0x284c
entry 0x4004c190
*wm:[1] AutoConnect 
*wm:[2] ESP32 event handler enabled 
*wm:[2] Connecting as wifi client... 
*wm:[2] setSTAConfig static ip not set, skipping 
*wm:[1] Connect Wifi, ATTEMPT # 1 of 2
*wm:[1] Connecting to SAVED AP: halebopp
*wm:[1] connectTimeout not set, ESP waitForConnectResult... 
*wm:[2] Connection result: WL_CONNECTED
*wm:[1] AutoConnect: SUCCESS 
*wm:[1] STA IP Address: 192.168.0.43
connected...yeey :)
initWebserver()
webserver started
Setting up ADC task
configuring ADC
eFuse Two Point: Supported
Characterized using Two Point Value
Raw: 3375       Voltage: 357mV
Raw: 3377       Voltage: 357mV
Raw: 3378       Voltage: 357mV
Raw: 3378       Voltage: 357mV
Raw: 3382       Voltage: 358mV
Raw: 3387       Voltage: 358mV
Raw: 3389       Voltage: 358mV
Raw: 3391       Voltage: 359mV
Raw: 3393       Voltage: 359mV
WiFi has lost connection, trying to reconnect
*wm:[1] AutoConnect 
Raw: 3395       Voltage: 359mV
*wm:[2] ESP32 event handler enabled 
*wm:[2] Connecting as wifi client... 
*wm:[2] setSTAConfig static ip not set, skipping 
*wm:[1] Connect Wifi, ATTEMPT # 1 of 2
*wm:[1] Connecting to SAVED AP: halebopp
*wm:[1] connectTimeout not set, ESP waitForConnectResult... 
Raw: 3396       Voltage: 359mV
Raw: 3397       Voltage: 359mV
*wm:[2] [EVENT] WIFI_REASON:  201
*wm:[2] [EVENT] WIFI_REASON: NO_AP_FOUND 
Raw: 3397       Voltage: 359mV
*wm:[2] Connection result: WL_NO_SSID_AVAIL
*wm:[1] Connect Wifi, ATTEMPT # 2 of 2
*wm:[1] Connecting to SAVED AP: halebopp
E (19071) wifi:sta is connecting, return error
[ 18662][E][WiFiSTA.cpp:241] begin(): connect failed! 0x3007
*wm:[1] connectTimeout not set, ESP waitForConnectResult... 
*wm:[2] Connection result: WL_NO_SSID_AVAIL
*wm:[1] AutoConnect: FAILED 
*wm:[2] Starting Config Portal 
*wm:[2] Disabling STA 
*wm:[2] Enabling AP 
*wm:[1] StartAP with SSID:  AutoConnectAP
*wm:[2] AP has anonymous access! 
Raw: 3400       Voltage: 359mV
*wm:[1] AP IP address: 192.168.4.1
*wm:[1] Starting Web Portal 
*wm:[2] HTTP server started 
*wm:[2] Config Portal Running, blocking, waiting for clients... 
Raw: 3400       Voltage: 359mV
```
